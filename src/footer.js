import React from 'react';
import { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
class Footer extends Component{
    render(){
    return(

<div className="footer pt-2 pb-4 bg-blue-footer text-black-50">
<div className="container text-center">
  <small>Copyright &copy; Your Website</small>
</div>
</div>
    )
    }
};
export default Footer
