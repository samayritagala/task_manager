import React , {Component} from 'react';
import  axios from 'axios'
import NavBarHome from './nav-home';
import SweetAlert from 'react-bootstrap-sweetalert';
import * as url from './app-config';
class RegisterUser extends Component {
    constructor(props) {
        super(props);




       // this.onSubmit = this.onSubmit.bind(this);
  
        this.state = {
            parent_journal:'',
            paper_title: '',
            taskPriority: '',
            specialization:'',
            research_file:'',
            research_file_name:'',
            primary_author_name:'',
            primary_author_institute:'',
            primary_author_mobile_no:'',
            primary_email_id:'',
            secondary_author_name:'',
            secondary_author_institute:'',
            secondary_author_mobile_no:'',
            secondary_author_email_id:'',
            specialization_list:[],
            area_list:[],
            alert:'',
            submitted: false,
            invalidEmailSec:false,
            invalidEmail:false,
            invalidMob:false,
            invalidMobSec:false

        }
        this.onChangePaperTitle = this.onChangePaperTitle.bind(this);
        this.onChangePrimaryAuthorName=this.onChangePrimaryAuthorName.bind(this);
        this.onChangeTaskPriority=this.onChangeTaskPriority.bind(this);
        this.onChangeUserName=this.onChangeUserName.bind(this);
        this.onChangeResearchSpecialization= this.onChangeResearchSpecialization.bind(this);
        this.onChangeResearchFile=this.onChangeResearchFile.bind(this);
        this.onChangePrimaryAuthorInstitute=this.onChangePrimaryAuthorInstitute.bind(this);
        this.onChangePrimaryAuthorMobileNo=this.onChangePrimaryAuthorMobileNo.bind(this);
        this.onChangePrimaryEmailId=this.onChangePrimaryEmailId.bind(this);
        this.onChangeSecondaryAuthorName=this.onChangeSecondaryAuthorName.bind(this);
        this.onChangeSecondaryAuthorMobileNo= this.onChangeSecondaryAuthorMobileNo.bind(this);
        this.onChangeSecondaryAuthorInstitute=this.onChangeSecondaryAuthorInstitute.bind(this);
        this.onChangeSecondaryAuthorEmailId=this.onChangeSecondaryAuthorEmailId.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
    }
    onChangePaperTitle(event) {
        this.setState({
          paper_title: event.target.value
        });
        console.log(event.target.value)
      }
      onChangePrimaryAuthorName(event) {
        this.setState({
            primary_author_name: event.target.value
          });
    }
    onChangeTaskPriority(event){
        this.setState({
            taskPriority: event.target.value
          });
    }
    onChangeUserName(event){
        this.setState({
            userName:event.target.value
        });
    }
    onChangeResearchSpecialization(event){
        this.setState({
            specialization: event.target.value
          });
    }
onChangeResearchFile(event){
    this.setState({
        research_file:event.target.files[0]
      });
      this.setState({
        research_file_name:event.target.files[0].name
      });
      console.log(event.target.files[0])
}

onChangePrimaryAuthorInstitute(event){
    this.setState({
        primary_author_institute: event.target.value
      });
}
onChangePrimaryAuthorMobileNo(event){
    this.setState({
        primary_author_mobile_no: event.target.value
      });
      if(!this.validatePhonenumber(event.target.value)){
        this.setState({
          invalidMob:true
        })
      } else {
        this.setState({
            invalidMob:false
        })
      }
}
onChangePrimaryEmailId(event){
    this.setState({
        primary_email_id: event.target.value
      });
      if(!this.validateEmail(event.target.value)){
        this.setState({
          invalidEmail:true
        })
      } else {
        this.setState({
          invalidEmail:false
        })
      }
}
onChangeSecondaryAuthorEmailId(event){
    this.setState({
        secondary_author_email_id: event.target.value
      });
      if(!this.validateEmail(event.target.value)){
        this.setState({
          invalidEmailSec:true
        })
      } else {
        this.setState({
          invalidEmailSec:false
        })
      }
}
onChangeSecondaryAuthorInstitute(event){
    this.setState({
        secondary_author_institute: event.target.value
      });
}
onChangeSecondaryAuthorName(event){
    this.setState({
        secondary_author_name: event.target.value
      });
}
onChangeSecondaryAuthorMobileNo(event){
    this.setState({
        secondary_author_mobile_no: event.target.value
      });
      if(!this.validatePhonenumber(event.target.value)){
        this.setState({
          invalidMobSec:true
        })
      } else {
        this.setState({
            invalidMobSec:false
        })
      }
}
componentDidMount(){
    this.get_specializationList();
    this.get_areaList();
}
validateEmail(email){
    const pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
    const result = pattern.test(email);
    return result;
   }
validatePhonenumber(inputtxt) {
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(phoneno.test(inputtxt)) {
      return true;
    }
    else {
      return false;
    }
  } 
   
onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    if(this.state.research_file &&this.state.paper_title && this.state.primary_email_id&&this.state.primary_author_institute
        &&this.state.specialization && this.state.research_area ){
            if (this.state.secondary_author_email_id) {
                let data = this.validateEmail(this.state.secondary_author_email_id);
                if (!data) {
                    const getAlert = () => (<SweetAlert error title="Validation Error" 
                    onConfirm={() => this.hideAlert()}>
                    Invalid Secondary Author Email
                     </SweetAlert>);

                this.setState({
                    alert: getAlert()
                });
                    return false;
                }
            }
            if (this.state.secondary_author_mobile_no) {
                let data = this.validatePhonenumber(this.state.secondary_author_mobile_no)
                if (!data) {
                    const getAlert = () => (<SweetAlert error title="Validation Error" 
                    onConfirm={() => this.hideAlert()}>
                    Invalid Secondary Author Mobile No
                     </SweetAlert>);

                this.setState({
                    alert: getAlert()
                });
                    return false
                }

            }
            if (this.state.primary_email_id) {
                let data = this.validateEmail(this.state.primary_email_id);
                if (!data) {
                    const getAlert = () => (<SweetAlert error title="Validation Error" 
                    onConfirm={() => this.hideAlert()}>
                    Invalid Primary Author Email
                     </SweetAlert>);

                this.setState({
                    alert: getAlert()
                });
                    return false;
                }
            }
            if (this.state.primary_author_mobile_no) {
                let data = this.validatePhonenumber(this.state.primary_author_mobile_no)
                if (!data) {
                    const getAlert = () => (<SweetAlert error title="Validation Error" 
                    onConfirm={() => this.hideAlert()}>
                    Invalid Primary Author Mobile No
                     </SweetAlert>);

                this.setState({
                    alert: getAlert()
                });
                    return false
                }

            }
   
    const formData = new FormData();
    const files = this.state.research_file;
    console.log(files)
    formData.append('uploads[]', files,files.name);
    axios.post(`${url.url}/upload`, formData, {
    })
    .then(res => {
       console.log(res.statusText)

    },err=>{

    })

   
    axios.post(`${url.url}/register-author`, this.state, {
        })
        .then(res => {
          console.log(res)
          const getAlert = () => (<SweetAlert success title="Registration Success!" onConfirm={() => this.hideAlert()}>
           Journal  registered successfully
                   </SweetAlert>);

          this.setState({
              alert: getAlert()
          });
          this.setState({
            paper_title: '',
            parent_journal:'',
            research_area: '',
            specialization:'',
            research_file:'',
            research_file_name:'',
            primary_author_name:'',
            primary_author_institute:'',
            primary_author_mobile_no:'',
            secondary_author_name:'',
            secondary_author_institute:'',
            secondary_author_mobile_no:'',
            primary_email_id:'',
            secondary_author_email_id:'',
        })
        this.props.history.push('/author-login');
        },err=>{
            const getAlert = () => (<SweetAlert error title="Registration Failed!" onConfirm={() => this.hideAlert()}>
            Register Journal failed
                     </SweetAlert>);
  
            this.setState({
                alert: getAlert()
            });
        })
        
} else {
    return false
}
}
  hideAlert() {
    console.log('Hiding alert...');
    this.setState({
        alert: null
    });
}

  get_areaList =() =>{
    axios.get(`${url.url}/area-list-findall`, {
    })
    .then(res => {
      debugger;
      console.log(res.data)
      
      this.setState({
       area_list: res.data
    })
    },err=>{
    
    })
   }
   get_specializationList =() =>{
    axios.get(`${url.url}/specialization-list-findall`, {
    })
    .then(res => {
      debugger;
      console.log(res.data)
      
      this.setState({
       specialization_list: res.data
    })
    },err=>{
    
    })
   }
    


    render() {
        return (
            <div>
                <NavBarHome></NavBarHome>
           
            <div className="container pt-3 pb-3">
            {this.state.alert}
            <form onSubmit={this.onSubmit}>
                <div className="panel-heading">
                    <h5 className="pt-3 pb-3">Add User</h5>
                </div>
                <fieldset className="content-feild-set">
                <div className={'form-group col-6' + ((this.state.submitted  && !this.state.userName) ? ' has-error' : '')} p>
                    <label>User Name </label>
                    <input type="text" className="form-control" value={this.state.userName} onChange={this.onChangeUserName}/>
                     {this.state.submitted && !this.state.userName &&
                            <div className="help-block errorMsg">User Name is required</div>
                     }
                </div>
               
               
                </fieldset>
                
                <div class="row pl-3 pt-2 pb-7">              
                <div className="form-group pb-5">
                <button type="submit" className="btn btn-primary">Submit</button>
                </div>
                
</div>
              
            </form>
        </div>
        </div>
        )
    }    
}

export default RegisterUser