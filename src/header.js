import React from 'react';
import { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
class Header extends Component{
    render(){
    return(
<nav className="navbar navbar-expand-lg  bg-dark text-white-50">
  <a className="navbar-brand" >Journal</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarText">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <a className="nav-link" >Home <span className="sr-only">(current)</span></a>
        
      </li>
      <li className="nav-item">
        <a className="nav-link" >Editor Board</a>
      </li>
      


      <li className="nav-item dropdown">
      <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#"  id="navbarDropdownoperations" role="button" aria-haspopup="true" aria-expanded="false">For Author</a>
        
        <div className="dropdown-menu"  aria-labelledby="navbarDropdownoperations">
          <a className="dropdown-item" href="#">Publication Guidelines</a>
          <a className="dropdown-item" href="#">Publication Charges</a>
          <a className="dropdown-item" href="#">Sample Paper Format</a>
          <a className="dropdown-item" href="#">Check Your Paper Status</a>
        </div>
      </li>
        <li className="nav-item active">
        <a className="nav-link" href="#">Submit Paper Online <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Archive</a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Current Issues</a>
          <a className="dropdown-item" href="#">Past Issues</a>
        </div>
        </li>
        <li className="nav-item active">
        <a className="nav-link" href="#">Contacts <span className="sr-only">(current)</span></a>
      </li>
      {/* <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownlogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Login</a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdownlogin">
          <Link className="dropdown-item" to="/editor-login">Editor Login</Link>
          <Link className="dropdown-item" to="/reviewer-login">Reviewer Login</Link>
        </div>
        </li> */}
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownissue" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Publish Issue Details</a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdownissue">
          <a className="dropdown-item" href="#">Current Issue</a>
          <a className="dropdown-item" href="#">Past Issue</a>
          <a className="dropdown-item" href="#">Special Issue</a>
          <a className="dropdown-item" href="#">Track Paper</a>
          </div>
        </li>



    </ul>
    <span className="navbar-text">
    
    <Link className="nav-link" to="/reviewer-login">Logout</Link>
    
    </span>
  </div>
</nav>
    )
    }
};
export default Header

