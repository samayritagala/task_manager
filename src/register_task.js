import React , {Component} from 'react';
import  axios from 'axios'
import NavBarHome from './nav-home';
import SweetAlert from 'react-bootstrap-sweetalert';
import * as url from './app-config';
class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            taskDescription: '',
            dueDate:'',
            taskPriority: '',
            assignedTo:''
        }
       
        this.onChangeTaskDescription=this.onChangeTaskDescription.bind(this);
        this.onChangeTaskPriority=this.onChangeTaskPriority.bind(this);
        this.onChangeDueDate=this.onChangeDueDate.bind(this);
        this.onChangeAssignedTo=this.onChangeAssignedTo.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
    }
    onChangeAssignedTo(event) {
        this.setState({
            assignedTo:event.target.value
        });
    }
    onChangeTaskDescription(event){
        this.setState({
            taskDescription:event.target.value
        });
    }
    onChangeTaskPriority(event){
        this.setState({
            taskPriority: event.target.value
          });
    }
    onChangeDueDate(event)
    {
        
        this.setState({
            
            dueDate:event.target.value
           });
    }
   
    getAssignedToList = () => {
        axios.get("https://devza.com/tests/tasks/listusers", { 
            headers: {
            "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
          }
        })
            .then(res => {
                debugger;
                console.log(res.data)

                this.setState({
                    assignedToList: res.data.users
                })
            }, err => {

            })
    }
    componentDidMount() {
        this.getAssignedToList();
    }
 
onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    if(this.state.taskDescription){
           
   
    const formData = new FormData();
    var selected_datetime = new Date(this.state.dueDate);
    var current_datetime = new Date();
    var formatted_date = selected_datetime.getFullYear() + "-" + (selected_datetime.getMonth() + 1) + "-" + selected_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" +current_datetime.getSeconds();
    formData.append('message',this.state.taskDescription);
    formData.append('due_date',formatted_date);
    formData.append('priority',this.state.taskPriority);
    formData.append('assigned_to',this.state.assignedTo);
    axios.post("https://devza.com/tests/tasks/create", formData, {
        headers: {
            "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
          }
        })
        .then(res => {
          console.log(res)
          const getAlert = () => (<SweetAlert success title="Task Assigned Successfully!" onConfirm={() => this.hideAlert()}>
           Task Assigned Successfully
                   </SweetAlert>);

          this.setState({
              alert: getAlert()
          });
          this.setState({
            taskDescription: '',
            taskPriority: '',
            assignedTo:''
        })
        
        },err=>{
            const getAlert = () => (<SweetAlert error title="Task Assigning Failed!" onConfirm={() => this.hideAlert()}>
            Task Assigning Failed
                     </SweetAlert>);
  
            this.setState({
                alert: getAlert()
            });
        })
        
} else {
    return false
}
}
  hideAlert() {
    console.log('Hiding alert...');
    this.setState({
        alert: null
    });
}

  
       render() {
        return (
            <div>
                <NavBarHome></NavBarHome>
           
            <div className="container pt-3 pb-3">
            {this.state.alert}
            <form onSubmit={this.onSubmit}>
                <div className="panel-heading">
                    <h5 className="pt-3 pb-3">Add Task</h5>
                </div>
                <fieldset className="content-feild-set">
                <div className={'form-group' + ((this.state.submitted  && !this.state.taskDescription) ? ' has-error' : '')} p>
                    <label>Task Detail </label>
                    <input type="text" className="form-control" value={this.state.taskDescription} onChange={this.onChangeTaskDescription}/>
                     {this.state.submitted && !this.state.taskDescription &&
                            <div className="help-block errorMsg">Task description is required</div>
                        }
                </div>
               
                <div className="form-group">
                    <div className="row">
                    <div className={'form-group col-4' + ((this.state.submitted  && !this.state.dueDate) ? ' has-error' : '')} p>
                    <label>Due Date </label>
                    <input type="date" className="form-control" value={this.state.dueDate} onChange={this.onChangeDueDate}/>                      
                    </div>
                        <div className="col-4">
                        <label>Priority </label>
                            <select className="form-control"  value={this.state.taskPriority}
                     onChange={this.onChangeTaskPriority} >
                     <option value="1">High</option>
                     <option value="2">Medium</option>
                     <option value="3">Low</option>
              
                            </select> 
                            {this.state.submitted && !this.state.taskPriority &&
                            <div className="help-block errorMsg">Priority is required</div>
                        }
                        </div>
                <div className="col-4">     
                    <label>Assigned To </label>
                    <select className="form-control" value={this.state.assignedTo}
                        onChange={this.onChangeAssignedTo} >
                        <option value="">Select</option>
                        {
                            (this.state.assignedToList && this.state.assignedToList.length > 0) && this.state.assignedToList.map((schema, key) => {
                                return (<option key={key} value={schema.id} > {schema.name}</option>);
                            })
                        }
                    </select>
                            {this.state.submitted && !this.state.assignedTo &&
                            <div className="help-block errorMsg">User is required</div>
                        }
                            </div>
                    </div>
                </div>
                </fieldset>
                
                <div class="row pl-3 pt-2 pb-7">              
                <div className="form-group pb-5">
                <button type="submit" className="btn btn-primary">Submit</button>
                </div>
                
</div>
              
            </form>
        </div>
        </div>
        )
    }    
}

export default Register