import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Register from './register_task';
import RegisterUser from './register_user';
import ViewTask from './view_task';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import { Provider } from 'react-redux';
import Footer from './footer';
import  { Redirect,BrowserRouter} from 'react-router-dom';
import { Link
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

import { history } from '../src/Services/history';

class App extends Component {

  constructor(props) {
    super(props);


    this.state = {
      currentUser: null,
      isAdmin: false
  };
    console.log(this.state,"yyyyyyyyy")
}




  componentWillMount(){
   
  }
  // componentDidMount(){
  // let auth = this.authenticatedUser();
  // this.setState({
  //   authenticated:auth
  // })
  // }
  // componentWillMount(){
  //   let auth = this.authenticatedUser();
  //   this.setState({
  //     authenticated:auth
  //   })
  // }
  logout() {
    let role= window.sessionStorage.getItem('role');
   // authenticationService.logout();
   window.sessionStorage.setItem('token','');
   window.sessionStorage.setItem('email','');
   window.sessionStorage.setItem('role','');
   this.contextTypes.router.history.push("/Dashboard") 
   // history.push('/login');
  if(role == 'editor'){
 //   this.props.history.push('/editor-login');
    return  <Redirect  to="/editor-login" />
  } else if(role === 'reviewer'){
    return  <Redirect  to="/reviewer-login" />
  } else if(role === 'author') {
    return  <Redirect  to="/author-login" />
  }
  
}
  render() {
    debugger;
  const { currentUser, isAdmin } = this.state;

  const  authenticatedMenu =  (<div><Provider >
      <div className='App'>
        <nav className="navbar navbar-expand-lg  bg-dark text-white-50" >
          <a className="navbar-brand" >Journal</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a onClick={this.logout_user} className="nav-item nav-link cursor-pointer" >Logout</a>
    </li>
  </ul>
          </div>
        </nav>
        <div className="container-fluid">
          <div className="row">
            <nav className="col-md-2 d-xs-block d-sm-block d-md-block bg-dark sidebar">
              <div className="sidebar-sticky">
                <ul className="nav flex-column">
                  <li className="nav-item">
                    <a className="nav-link active" href="#">
                      <span data-feather="home"></span>
                      Dashboard <span className="sr-only">(current)</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      <span data-feather="file"></span>
                      Orders
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      <span data-feather="shopping-cart"></span>
                      Products
                    </a>
                  </li>
  
                </ul>
              </div>
            </nav>
  
            <BrowserRouter history={history}>
              <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
  
                
              </main>
            </BrowserRouter>
  
          </div>
        </div>
        <Footer></Footer>
      </div>
    </Provider>
    </div>
   );
  const nonauthenticated =(
    <div>
  
  <BrowserRouter  history={history}>

        <Switch>
      
        <Route path='/add-task' component={Register} />
        <Route path='/view-task' component={ViewTask} />
        <Route path='/' component={ViewTask} />
    {/* <Route path='/edit/:id' component={ Edit } /> */}
    
  </Switch>
  
  </BrowserRouter>
  <Footer></Footer>
  </div>
   );
    return (
     <div>
     {currentUser ? nonauthenticated : nonauthenticated}
     </div>
    )
}
}

export default App;