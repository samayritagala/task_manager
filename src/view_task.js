import React , {Component} from 'react';
import  axios from 'axios';
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import NavBarHome from './nav-home';
import  * as url from './app-config'
class ViewTask extends Component {
    constructor(props) {
        super(props);
       // this.onSubmit = this.onSubmit.bind(this);
  
        this.state = {
            alert:'',
            taskid:'',
            taskDescription: '',
            dueDate:'',
            taskPriority: '',
            assignedTo:'',
            submitted_form:false,
            show_hide_grid:true,
            show_hide_form:false,
            columnDefs: [
                {
                    headerName: "Task Name", field: "message", sortable: true, filter: "agTextColumnFilter", sortable: true,
                  }, 
                    {
                    headerName: "Priority", field: "priority", filter: "agTextColumnFilter", sortable: true,
                    cellRenderer: (value) => {
                      debugger;
                     
                      return value.value
                    }
                    },
                    {
                        headerName: "Due Date", field: "due_date", filter: "agTextColumnFilter", sortable: true,
                       
                    }, 
                    {
                        headerName: "Assigned To", field: "assigned_name", sortable: true, filter: "agTextColumnFilter", sortable: true,
                    },
                    {
                      headerName: "Edit",
                      template:
                      `<button type="button"  data-action-type="edit" class="btn btn-sm btn-primary">
                      <i data-action-type="edit" class="fa fa-pencil"></i></button>`
                    },
                    {
                      headerName: "Delete",
                      template:
                      `<button type="button"  data-action-type="delete" class="btn btn-sm btn-danger">
                      <i data-action-type="delete" class="fa fa-trash"></i></button>`
                    }

            ],
            defaultColDef: { filter: true ,
                enableRowGroup: true,
                enablePivot: true,
                enableValue: true,
                sortable: true,
                resizable: true,
                filter: true },
      rowData: null,
      frameworkComponents: { },
      settingList:[]

        }
        
        
        this.onChangeTaskDescription=this.onChangeTaskDescription.bind(this);
        this.onChangeTaskPriority=this.onChangeTaskPriority.bind(this);
        this.onChangeDueDate=this.onChangeDueDate.bind(this);
        this.onChangeAssignedTo=this.onChangeAssignedTo.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
    }

    onChangeAssignedTo(event) {
      this.setState({
          assignedTo:event.target.value
      });
  }
  onChangeTaskDescription(event){
      this.setState({
          taskDescription:event.target.value
      });
  }
  onChangeTaskPriority(event){
      this.setState({
          taskPriority: event.target.value
        });
  }
  onChangeDueDate(event)
  {
      
      this.setState({
          
          dueDate:event.target.value
         });
  }
   
    cancel_form= () =>{
        this.setState({
          show_hide_grid :true,
          show_hide_form:false
        })
    }
    onGridReady(params) {
        let rowData
        axios.get("https://devza.com/tests/tasks/list",{ 
          headers: {
          "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
        }
      })
        .then(data => {
          console.log(data);
            
            data.data.tasks.map((data)=>{
                if(data.priority=="1")
                {
                   data.priority="High";
                }
                else if(data.priority=="2")
                {
                  data.priority="Medium";
                }
                else if(data.priority=="3")
                {
                  data.priority="Low";
                }
                else{
                  data.priority="-";
                }
            })
            rowData = data.data.tasks;
            this.api = params.api;
            if ( rowData.length > 0 && this.api ) {
              this.api.setRowData( rowData );
              this.gridApi = params.api;
              this.gridColumnApi = params.columnApi;
              params.api.sizeColumnsToFit();
            } else {
                this.api.setRowData( [] );    
            }
        }
     )
    }
    refresh_data = ()=>{
      let rowData
      axios.get("https://devza.com/tests/tasks/list",{ 
        headers: {
        "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
      }
    })
      .then(data => {
           rowData = data.data.tasks;
          if ( rowData.length > 0 ) {
            this.setState({
              rowData:rowData,
                show_hide_form:false,
                show_hide_grid:true,
            })
    }
  })
}
getAssignedToList = () => {
  axios.get("https://devza.com/tests/tasks/listusers", { 
      headers: {
      "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
    }
  })
      .then(res => {
          debugger;
          console.log(res.data)

          this.setState({
              assignedToList: res.data.users
          })
      }, err => {

      })
}

componentDidMount(){
  this.getAssignedToList();
  
}

  hideAlert() {
    console.log('Hiding alert...');
    this.setState({
        alert: null
    });
}
onSubmit(e) {
    e.preventDefault();
    this.setState({
        submitted_form:true
    })
    if(this.state.taskDescription){
           
   
        const formData = new FormData();
        var selected_datetime = new Date(this.state.dueDate);
        var current_datetime = new Date();
        var formatted_date = selected_datetime.getFullYear() + "-" + (selected_datetime.getMonth() + 1) + "-" + selected_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" +current_datetime.getSeconds();
        formData.append('message',this.state.taskDescription);
        formData.append('due_date',formatted_date);
        formData.append('priority',this.state.taskPriority);
        formData.append('assigned_to',this.state.assignedTo);
        formData.append('taskid',this.state.taskid);
        axios.post("https://devza.com/tests/tasks/update", formData, {
            headers: {
                "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
              }
            })
            .then(res => {
              console.log(res)
              const getAlert = () => (<SweetAlert success title="Task Updated Successfully!" onConfirm={() => this.hideAlert()}>
              Task Updated Successfully
                      </SweetAlert>);
    
              this.setState({
                  alert: getAlert()
              });
              this.setState({
                taskDescription: '',
                taskPriority: '',
                assignedTo:'',
                taskid:''
            })
            
            },err=>{
                const getAlert = () => (<SweetAlert error title="Task Assigning Failed!" onConfirm={() => this.hideAlert()}>
                Task Updation Failed
                        </SweetAlert>);
      
                this.setState({
                    alert: getAlert()
                });
            })
            
    } else {
        return false
    }

        }
        cancel_form= () =>{
            this.setState({
              show_hide_grid :true,
              show_hide_form:false
            })
        }
        show_form= () =>{
            this.setState({
              show_hide_grid :false,
              show_hide_form:true
            })
        }
        
        onRowClicked=(e)=> {
            if (e.event.target !== undefined) {
                let data = e.data;
                let actionType = e.event.target.getAttribute("data-action-type");
               
                const formData = new FormData();
                
                if(actionType=="edit") {

                     
                        debugger;
                        
                        if(data['priority']==="High")
                        {
                          data.priority=1;
                        }
                        else if(data['priority']==="Medium")
                        {
                          data.priority=2;
                        }
                        else if(data['priority']==="Low")
                        {
                          data.priority=3;
                        }
                        var selected_date = new Date(data['due_date']);
                        var dd = selected_date.getDate(); 
                        var mm = selected_date.getMonth() + 1; 
                  
                        var yyyy = selected_date.getFullYear(); 
                        if (dd < 10) { 
                            dd = '0' + dd; 
                        } 
                        if (mm < 10) { 
                            mm = '0' + mm; 
                        } 
                        var selectedDay = dd + '/' + mm + '/' + yyyy; 
                        
                        this.setState({
                          taskDescription:data['message'],
                          taskPriority:data['priority'],
                          dueDate:selectedDay,
                          assignedTo:data['assigned_to'],
                          taskid:data['id'],
                          
                            show_hide_grid :false,
                            show_hide_form:true,
                            
                        })
                      }

                      else if(actionType=="delete")
                      {
                        formData.append('taskid',data.id);
                        axios.post("https://devza.com/tests/tasks/delete" ,formData 
                        , {
                          headers: {
                            "AuthToken": "7WUy6aG7kNY0BvonPisS7SOHzbegvUDp"
                          }
                          })
                         .then(res => {
                             const getAlert = () => (<SweetAlert success title="Success!" onConfirm={() => this.hideAlert()}>
                             Data deleted Successfully
                            </SweetAlert>);
         
                            this.setState({
                                alert: getAlert()
                            })
                            this.refresh_data();
                         }, err => {
                             debugger;
                             const getAlert = () => (<SweetAlert error title="Failed!" onConfirm={() => this.hideAlert()}>
                                             Data deletion failed
                                  </SweetAlert>);
                 
                                         this.setState({
                                             alert: getAlert()
                                         });
                         })
                      }
                        
               
            }
        }
        onColumnResized =(params) =>{

            this.gridApi.sizeColumnsToFit();
            if (params.source === 'uiColumnDragged' && params.finished) {
                params.api.sizeColumnsToFit();
              }
        }
        gridSizeChanged=(params)=>{
            params.api.sizeColumnsToFit();
        }
        firstDataRendered=(params)=>{
            params.api.sizeColumnsToFit();  
        }   
       
    render() {
        const {show_hide_form,show_hide_grid}=this.state;
        return (
            <div className='App'>
        <NavBarHome></NavBarHome>
        <div className="container-fluid">
          <div className="row">
          
                  <main role="main" className="col-md-12 ml-sm-auto col-lg-10 pt-3 px-4">
{this.state.alert}
            <div class="pt-3 pb-3 ">
           

                              { show_hide_grid && (
    <div className="row">
        <div className="col-12 pt-3 pb-4">
            <h5 className="pt-3 pb-3">View Task</h5>
        <div style={{ height: '500px', width: '100%' }} className="ag-theme-material">
    <AgGridReact
        columnDefs={this.state.columnDefs}
        rowData={this.state.rowData}
        onGridReady={this.onGridReady}
        onRowClicked ={this.onRowClicked}
        floatingFilter={true}
        frameworkComponents={this.state.frameworkComponents}
        columnResized={this.onColumnResized}
        firstDataRendered={this.firstDataRendered}
        gridSizeChanged={this.gridSizeChanged}
        pagination={true}
        paginationPageSize={10}>
        
       
    </AgGridReact>
     </div>
   </div>
</div>)}
{ show_hide_form && (

    <div className="row pt-3 pb-3">
    <div className="col-12">
            
            <form onSubmit={this.onSubmit}>
                <div className="panel-heading">
                    <h5 className="pt-3 pb-3">Update Task</h5>
                </div>
                <fieldset className="content-feild-set">
                <div className={'form-group' + ((this.state.submitted  && !this.state.taskDescription) ? ' has-error' : '')} p>
                    <label>Task Detail </label>
                    <input type="text" className="form-control" value={this.state.taskDescription} onChange={this.onChangeTaskDescription}/>
                     {this.state.submitted && !this.state.taskDescription &&
                            <div className="help-block errorMsg">Task description is required</div>
                        }
                </div>
               
                <div className="form-group">
                    <div className="row">
                    <div className={'form-group col-4' + ((this.state.submitted  && !this.state.dueDate) ? ' has-error' : '')} p>
                    <label>Due Date </label>
                    <input type="text" className="form-control" value={this.state.dueDate} onChange={this.onChangeDueDate}/>                      
                    </div>
                        <div className="col-4">
                        <label>Priority </label>
                            <select className="form-control"  value={this.state.taskPriority}
                     onChange={this.onChangeTaskPriority} >
                     <option value="1">High</option>
                     <option value="2">Medium</option>
                     <option value="3">Low</option>
              
                            </select> 
                            {this.state.submitted && !this.state.taskPriority &&
                            <div className="help-block errorMsg">Priority is required</div>
                        }
                        </div>
                <div className="col-4">     
                    <label>Assigned To </label>
                    <select className="form-control" value={this.state.assignedTo}
                        onChange={this.onChangeAssignedTo} >
                        <option value="">Select</option>
                        {
                            (this.state.assignedToList && this.state.assignedToList.length > 0) && this.state.assignedToList.map((schema, key) => {
                                return (<option key={key} value={schema.id} > {schema.name}</option>);
                            })
                        }
                    </select>
                            {this.state.submitted && !this.state.assignedTo &&
                            <div className="help-block errorMsg">User is required</div>
                        }
                            </div>
                    </div>
                </div>
                </fieldset>
                
                <div class="row pl-3 pt-2 pb-5">
                <div className="form-group ">
                <button type="submit" className="btn btn-primary">Save</button>
                    <button type="button" className="btn btn-default" onClick={this.cancel_form}>Cancel</button>
                </div>
                
            </div>
              
            </form>
        </div>
        </div>)}
        </div>
       </main>
       </div>
       </div>
       </div>
        )
        
    }    
}
export default ViewTask