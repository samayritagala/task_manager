import React from 'react';
import { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class NavBarHome extends Component{
    render(){
    return(
<nav className="navbar navbar-expand-lg  bg-blue text-white-50">

  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarText">
    <ul className="navbar-nav mr-auto">
      
        
        <li className="nav-item">
        <Link className="nav-link" to="/add-task">Add Task</Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link" to="/view-task">View Task</Link>
        </li>


    </ul>
    <form class="navbar-form form-inline">
			<div class="input-group search-box">								
				{/* <input type="text" id="search" class="form-control" placeholder="Search here..."/> */}
				
			</div>
		</form>
      
  </div>
</nav>








    )
    }
};
export default NavBarHome

